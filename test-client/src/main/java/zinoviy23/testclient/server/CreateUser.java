package zinoviy23.testclient.server;

import com.google.gson.*;
import zinoviy23.lightclient.crypto.CryptoHash;
import zinoviy23.lightclient.crypto.CryptoPublicKey;
import zinoviy23.lightclient.crypto.CryptoSignature;
import zinoviy23.lightclient.transactions.PreparedTransaction;
import zinoviy23.lightclient.types.AbstractHash;

import java.lang.reflect.Type;

public class CreateUser {
    private ForServer server;
    private PreparedTransaction blockchain;

    public CreateUser(ForServer server, PreparedTransaction blockchain) {
        this.server = server;
        this.blockchain = blockchain;
    }

    @Override
    public String toString() {
        return PreparedTransaction.BUILDER_FOR_TX
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }
}
