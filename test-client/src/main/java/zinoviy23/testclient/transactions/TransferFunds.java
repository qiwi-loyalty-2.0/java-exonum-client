package zinoviy23.testclient.transactions;

import zinoviy23.lightclient.crypto.CryptoPublicKey;
import zinoviy23.lightclient.transactions.Transaction;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class TransferFunds extends Transaction {
    private CryptoPublicKey from;
    private CryptoPublicKey to;
    private long amount;
    private long seed;

    public TransferFunds(CryptoPublicKey from, CryptoPublicKey to, long amount, long seed) {
        super((byte) 0, (byte) 0, (short) 1, (short) 1);
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.seed = seed;
    }
}
