package zinoviy23.testclient.transactions;

import com.google.gson.annotations.SerializedName;
import zinoviy23.lightclient.crypto.CryptoPublicKey;
import zinoviy23.lightclient.transactions.Transaction;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class CreateWallet extends Transaction {
    @SerializedName("pub_key")
    private CryptoPublicKey publicKey;

//    private String name;

    public CreateWallet(CryptoPublicKey publicKey) {
        super((byte) 0, (byte) 0, (short) 0, (short) 1);
        this.publicKey = publicKey;
//        this.name = name;
    }
}
