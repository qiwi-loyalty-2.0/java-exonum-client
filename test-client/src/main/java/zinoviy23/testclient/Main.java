package zinoviy23.testclient;

import zinoviy23.lightclient.crypto.CryptoKeyPair;
import zinoviy23.lightclient.requests.BadRequestCodeException;
import zinoviy23.lightclient.requests.RequestBuilder;
import zinoviy23.lightclient.transactions.PreparedTransaction;
import zinoviy23.testclient.server.CreateUser;
import zinoviy23.testclient.server.ForServer;
import zinoviy23.testclient.transactions.CreateWallet;
import zinoviy23.testclient.transactions.TransferFunds;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("Duplicates")
public class Main {
//    private static final String transactionEndPoint = "http://34.211.0.61:8000/api/services/qiwiloyalty/v1/users";
    private static final String transactionEndPoint = "http://0.0.0.0:8000/api/services/qiwiloyalty/v1/users";
//    private static final String walletsEndPoint = "http://34.211.0.61:8000/api/services/qiwiloyalty/v1/users";
    private static final String walletsEndPoint = "http://0.0.0.0:8000/api/services/qiwiloyalty/v1/users";
    private static final String transferEndPoint =
            "http://34.211.0.61:8080/api/services/qiwiloyalty/v1/wallets/transfer";

    private static final String serverCreateUser = "http://18.223.152.25:8000/register/?format=json";

    private static RequestBuilder.OnSuccessHandler<String> onSuccessHandler = (inputDataStream) -> {
        BufferedReader br = new BufferedReader(inputDataStream);
        StringBuilder result = br.lines()
                .reduce(new StringBuilder(), StringBuilder::append, StringBuilder::append);

        return result.toString();
    };

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        if (args.length == 0) {
            CryptoKeyPair pairSasha = CryptoKeyPair.generateKeyPair();
            CryptoKeyPair pairPolina = CryptoKeyPair.generateKeyPair();

            CreateWallet walletSasha = new CreateWallet(pairSasha.getPublicKey());
//        CreateWallet walletPolina = new CreateWallet(pairPolina.getPublicKey(), "Polina");



            Future<String> createdSashasWallet = RequestBuilder.endPoint(transactionEndPoint)
                    .postTransaction()
                    .parameters(new PreparedTransaction(walletSasha, pairSasha.getPrivateKey()))
                    .onSuccess(onSuccessHandler)
                    .onError((Throwable::printStackTrace))
                    .send();

//        Future<String> createdPolinasWallet = RequestBuilder.endPoint(transactionEndPoint)
//                .postTransaction()
//                .parameters(new PreparedTransaction(walletPolina, pairPolina.getPrivateKey()))
//                .onSuccess(onSuccessHandler)
//                .onError((Throwable::printStackTrace))
//                .send();

//        System.out.println(createdPolinasWallet.get());
            System.out.println(createdSashasWallet.get());

//        TransferFunds transferFunds = new TransferFunds(pairSasha.getPublicKey(), pairPolina.getPublicKey(),
//                10, 0);

//        PreparedTransaction preparedTransaction = new PreparedTransaction(transferFunds, pairSasha.getPrivateKey());
//        Future<String> sendedFunds = RequestBuilder.endPoint(transferEndPoint)
//                .postTransaction()
//                .parameters(preparedTransaction)
//                .onSuccess(onSuccessHandler)
//                .onError((Throwable::printStackTrace))
//                .send();

//        System.out.println(sendedFunds.get());

            TimeUnit.MILLISECONDS.sleep(100);// blockchain needs some time for update db

            Future<String> walletsInfo = RequestBuilder.endPoint(walletsEndPoint)
                    .get()
                    .parameters(Collections.emptyMap())
                    .onSuccess(onSuccessHandler)
                    .onError((Throwable::printStackTrace))
                    .send();

            System.out.println(walletsInfo.get());

            RequestBuilder.shutdown();
        } else if (args[0].equals("server")) {
            CryptoKeyPair pairSasha = CryptoKeyPair.generateKeyPair();
            CreateWallet walletSasha = new CreateWallet(pairSasha.getPublicKey());

            CreateUser createUser = new CreateUser(
                    new ForServer(
                            "zinoviy234",
                            "zinoviy23@gmail4.com",
                            "Sasha",
                            "Izmaylov",
                            "blockchain-kek"),
                    new PreparedTransaction(walletSasha, pairSasha.getPrivateKey()));

            System.out.println(createUser);

            Future<String> createUserResponse = RequestBuilder
                    .endPoint(serverCreateUser)
                    .<CreateUser>post("application/json")
                    .parameters(createUser)
                    .onSuccess(onSuccessHandler)
                    .onError(ex -> {
                        System.out.println(ex);
                        if (ex instanceof BadRequestCodeException) {
                            try {
                                System.out.println(onSuccessHandler.onSuccess(
                                        ((BadRequestCodeException) ex).getErrorReader()));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .send();

            System.out.println(createUserResponse.get());

            RequestBuilder.shutdown();
        } else {
            int cnt = Integer.parseInt(args[0]);
            for (int i = 0; i < cnt; i++) {
                CryptoKeyPair pair = CryptoKeyPair.generateKeyPair();
                System.out.println(pair.getPublicKey());
                System.out.println(pair.getPrivateKey());
            }
        }
    }
}
