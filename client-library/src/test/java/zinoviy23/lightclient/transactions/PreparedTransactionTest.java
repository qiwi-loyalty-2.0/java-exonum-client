package zinoviy23.lightclient.transactions;

import org.junit.jupiter.api.Test;
import zinoviy23.lightclient.crypto.CryptoPublicKey;
import zinoviy23.lightclient.crypto.CryptoSecretKey;
import zinoviy23.lightclient.serialization.OrderId;

import static org.junit.jupiter.api.Assertions.*;

class PreparedTransactionTest {

    static class CreateWallet extends Transaction {
        @OrderId(id = 1)
        String name;

        @OrderId(id = 2)
        CryptoPublicKey publicKey;

        private CreateWallet(String name, CryptoPublicKey publicKey) {
            super((byte) 0, (byte) 0, (short) 0, (short) 1);
            this.name = name;
            this.publicKey = publicKey;
        }
    }

    @Test
    void prepareTransaction() {
        PreparedTransaction preparedTransaction = new PreparedTransaction(new CreateWallet("Sasha",
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a")),
                new CryptoSecretKey("978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b" +
                        "fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"));

        assertEquals("{\"body\":" +
                "{" +
                "\"name\":\"Sasha\"," +
                "\"publicKey\":\"fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a\"" +
                "}," +
                "\"protocol_version\":0," +
                "\"message_id\":0," +
                "\"service_id\":1," +
                "\"signature\":" +
                "\"d4b27b3ca2d11c2c6551d603b4490378769aeb3c0e9d427d5f36bb43e894cff4d8785585bbbb171f971beca185c796a6" +
                "44588b1d74481e0f0009429f72351700\"}", preparedTransaction.toJson());
    }
}