package zinoviy23.lightclient.transactions;

import org.junit.jupiter.api.Test;
import zinoviy23.lightclient.crypto.CryptoPublicKey;
import zinoviy23.lightclient.serialization.OrderId;
import zinoviy23.lightclient.types.ExonumSlice;
import zinoviy23.lightclient.types.ExonumStruct;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unused")
class TransactionTest {

    private static int[] rawTestData = {
            0, 0, 128, 0, 130, 0, 146, 0, 0, 0, 245, 96, 42, 104, 104, 7, 251, 245, 75, 71, 235, 76, 150, 181, 186, 195,
            53, 42, 68, 231, 80, 15, 110, 80, 123, 139, 78, 52, 19, 2, 199, 153, 247, 234, 143, 208, 44, 180, 28, 194,
            205, 69, 253, 90, 220, 137, 202, 27, 246, 5, 178, 227, 31, 121, 106, 52, 23, 221, 188, 212, 163, 99, 70, 71,
            232, 3, 0, 0, 0, 0, 0, 0
    };

    private static int[] rawTestData2 = {
            0, 0, 128, 0, 130, 0, 120, 0, 0, 0, 18, 0, 0, 0, 2, 0, 0, 0, 34, 0, 0, 0, 11, 0, 0, 0, 45, 0, 0, 0, 11,
            0, 0, 0, 8, 0, 0, 0, 3, 0, 0, 0, 75, 101, 107, 8, 0, 0, 0, 3, 0, 0, 0, 76, 111, 108
    };

    private static int[] rawTestData3 = {
            0, 0, 128, 0, 130, 0, 87, 0, 0, 0, 18, 0, 0, 0, 5, 0, 0, 0, 83, 97, 115, 104, 97
    };

    private static Byte[] testData, testData2, testData3;

    static {
        testData = new Byte[rawTestData.length];
        for (int i = 0; i < rawTestData.length; i++)
            testData[i] = (byte) rawTestData[i];

        testData2 = new Byte[rawTestData2.length];
        for (int i = 0; i < rawTestData2.length; i++)
            testData2[i] = (byte) rawTestData2[i];

        testData3 = new Byte[rawTestData3.length];
        for (int i = 0; i < rawTestData3.length; i++)
            testData3[i] = (byte) rawTestData3[i];
    }

    @Test
    void serialize() {
        class SendFunds extends Transaction {
            @OrderId(id = 1)
            private CryptoPublicKey from;
            @OrderId(id = 2)
            private CryptoPublicKey to;
            @OrderId(id = 3)
            private long amount;

            private SendFunds(byte networkId, byte protocolVersion, short messageId, short serviceId, CryptoPublicKey from,
                              CryptoPublicKey to, long amount) {
                super(networkId, protocolVersion, messageId, serviceId);
                this.from = from;
                this.to = to;
                this.amount = amount;
            }
        }

        SendFunds sendFunds = new SendFunds((byte) 0, (byte) 0, (short) 128, (short) 130,
                new CryptoPublicKey("f5602a686807fbf54b47eb4c96b5bac3352a44e7500f6e507b8b4e341302c799"),
                new CryptoPublicKey("f7ea8fd02cb41cc2cd45fd5adc89ca1bf605b2e31f796a3417ddbcd4a3634647"),
                1000);

        List<Byte> data = sendFunds.serialize();
        assertArrayEquals(testData, data.toArray(new Byte[0]));
    }

    @Test
    void serialize2() {
        @ExonumStruct
        class Bonus {
            @OrderId(id = 1)
            private String name;

            private Bonus(String name) {
                this.name = name;
            }
        }

        class GetWallet extends Transaction {
            @OrderId(id = 1)
            private ExonumSlice<Bonus> bonuses;

            private GetWallet(ExonumSlice<Bonus> bonuses) {
                super((byte) 0, (byte) 0, (short) 128, (short) 130);
                this.bonuses = bonuses;
            }
        }

        GetWallet tx = new GetWallet(new ExonumSlice<>(Bonus.class,
                                                       Arrays.asList(new Bonus("Kek"), new Bonus("Lol"))));

        List<Byte> bytes = tx.serialize();
        System.out.println(Arrays.toString(testData2));
        System.out.println(bytes);
        assertArrayEquals(testData2, bytes.toArray(new Byte[0]));
    }

    @Test
    void serialize3() {
        class CreateWallet extends Transaction {
            @OrderId(id = 1)
            private String name;

            private CreateWallet(String name) {
                super((byte) 0, (byte) 0, (short) 128, (short) 130);
                this.name = name;
            }
        }

        CreateWallet tx = new CreateWallet("Sasha");

        List<Byte> bytes = tx.serialize();
        System.out.println(Arrays.toString(testData3));
        System.out.println(bytes);
        assertArrayEquals(testData3, bytes.toArray(new Byte[0]));
    }
}