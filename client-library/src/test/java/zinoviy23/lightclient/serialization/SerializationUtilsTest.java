package zinoviy23.lightclient.serialization;

import org.junit.jupiter.api.Test;
import zinoviy23.lightclient.crypto.CryptoPublicKey;
import zinoviy23.lightclient.types.ExonumSlice;
import zinoviy23.lightclient.types.ExonumStruct;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unused")
class SerializationUtilsTest {

    @Test
    void serializeByte() {
        List<Byte> data = new ArrayList<>(Collections.nCopies(1, (byte)0));
        SerializationUtils.serialize((byte) (0xFF - 1), 0, data);
        assertEquals((byte)0xFF - 1, (byte)data.get(0));
    }

    @Test
    void serializeShort() {
        List<Byte> data = new ArrayList<>(Collections.nCopies(2, (byte)0));
        SerializationUtils.serialize((short) ((1 << 16) - 1 - (1 << 8) - 1), 0, data);
        for (byte b : data) {
            assertEquals((byte) (0xFF - 1), b);
        }
    }

    @Test
    void serializeInt() {
        List<Byte> data = new ArrayList<>(Collections.nCopies(4, (byte)0));
        SerializationUtils.serialize((-2 - (1 << 24) - (1 << 16) - (1 << 8)), 0, data);
        for (byte b : data) {
            assertEquals((byte) (0xFF - 1), b);
        }
    }

    @Test
    void serializeLong() {
        List<Byte> data = new ArrayList<>(Collections.nCopies(8, (byte)0));
        SerializationUtils.serialize(-2L - (1L << 56) - (1L << 48) - (1L << 40) - (1L << 32) - (1L << 24)
                - (1L << 16) - (1L << 8), 0, data);
        for (byte b : data) {
            assertEquals((byte) (0xFF - 1), b);
        }
    }

    @Test
    void serializeBoolean() {
        List<Byte> data = new ArrayList<>(Collections.nCopies(2, (byte) 0));
        SerializationUtils.serialize(true, 0, data);
        SerializationUtils.serialize(false, 1, data);

        assertEquals((byte) 0x01, (byte) data.get(0));
        assertEquals((byte) 0x00, (byte) data.get(1));
    }

    @Test
    void serializeFixedSize() {
        byte[] bytes = new byte[CryptoPublicKey.CRYPTO_PUBLIC_KEY_SIZE];
        Random random = new Random();

        for (int i = 0; i < bytes.length; i++)
            bytes[i] = (byte) random.nextInt();

        CryptoPublicKey key = new CryptoPublicKey(bytes);

        List<Byte> data = new ArrayList<>(Collections.nCopies(CryptoPublicKey.CRYPTO_PUBLIC_KEY_SIZE, (byte) 0));
        SerializationUtils.serialize(key, 0, data);

        for (int i = 0; i < bytes.length; i++)
            assertEquals(bytes[i], (byte)data.get(i));
    }

    @Test
    void serializeString() {
        String tmp = "AB";

        List<Byte> data = new ArrayList<>();

        SerializationUtils.SegmentInfo info = SerializationUtils.serialize(tmp, data);

        assertEquals(0, info.getStart());
        assertEquals(2, info.getLength());
        assertEquals(2, data.size());
        assertEquals((byte) 'A', (byte) data.get(0));
        assertEquals((byte) 'B', (byte) data.get(1));
    }

    @Test
    void serializeSimpleStructure() {
        @ExonumStruct
        class SimpleStructure {
            @OrderId(id = 1)
            private String b;

            @OrderId(id = 2)
            private int a;

            private SimpleStructure(int a, String b) {
                this.a = a;
                this.b = b;
            }
        }

        SimpleStructure structure = new SimpleStructure(10, "AB");

        List<Byte> data = new ArrayList<>();

        SerializationUtils.SegmentInfo info = SerializationUtils.serialize(structure, data);

        assertEquals(0, info.getStart());
        assertEquals(14, info.getLength());

        Byte[] testData = {12, 0, 0, 0, 2, 0, 0, 0, 10, 0, 0, 0, 65, 66};
        assertArrayEquals(testData, data.toArray(new Byte[]{}));
    }

    @Test
    void serializeNestedStructure() {
        @ExonumStruct
        class A {
            @OrderId(id = 1)
            private short first;

            @OrderId(id = 2)
            private short second;

            @OrderId(id = 3)
            private String a;

            private A(short first, short second, String a) {
                this.first = first;
                this.second = second;
                this.a = a;
            }
        }

        @ExonumStruct
        class B {
            @OrderId(id = 1)
            private A a;

            @OrderId(id = 2)
            private A b;

            @OrderId(id = 3)
            private String name;

            @OrderId(id = 4)
            private short kek;

            private B(A a, A b, String name, short kek) {
                this.a = a;
                this.b = b;
                this.name = name;
                this.kek = kek;
            }
        }

        B b = new B(
                new A((short) 11, (short) 22, "B"),
                new A((short) 33, (short) 44, "C"),
                "A",
                (short) 111);

        Byte[] testData = {26, 0, 0, 0, 13, 0, 0, 0, 39, 0, 0, 0, 13, 0, 0, 0, 52, 0, 0, 0, 1, 0, 0, 0, 111, 0, 11, 0,
                22, 0, 12, 0, 0, 0, 1, 0, 0, 0, 66, 33, 0, 44, 0, 12, 0, 0, 0, 1, 0, 0, 0, 67, 65};

        List<Byte> data = new ArrayList<>();
        SerializationUtils.serialize(b, data);
        System.out.println(Arrays.toString(testData));
        System.out.println(data);

        assertArrayEquals(testData, data.toArray(new Byte[] {}));
    }

    @Test
    void serializeNestedStructures2() {
        @ExonumStruct
        class A{
            @OrderId(id = 1)
            private short first;

            @OrderId(id = 2)
            private short second;

            @OrderId(id = 3)
            private String a;

            private A(short first, short second, String a) {
                this.first = first;
                this.second = second;
                this.a = a;
            }
        }

        @ExonumStruct
        class B {
            @OrderId(id = 1)
            private A a;

            @OrderId(id = 2)
            private A b;

            @OrderId(id = 3)
            private String name;

            @OrderId(id = 4)
            private short kek;

            private B(A a, A b, String name, short kek) {
                this.a = a;
                this.b = b;
                this.name = name;
                this.kek = kek;
            }
        }

        @ExonumStruct
        class C {
            @OrderId(id = 1)
            private B b;

            @OrderId(id = 2)
            private short c;

            private C(B b, short c) {
                this.b = b;
                this.c = c;
            }
        }

        B b = new B(
                new A((short) 11, (short) 22, "B"),
                new A((short) 33, (short) 44, "C"),
                "A",
                (short) 111);

        C c = new C(b, (short)10);

        Byte[] testData = {
                10, 0, 0, 0, 53, 0, 0, 0, 10, 0, 26, 0, 0, 0, 13, 0, 0, 0, 39, 0, 0, 0, 13, 0, 0, 0, 52, 0,
                0, 0, 1, 0, 0, 0, 111, 0, 11, 0, 22, 0, 12, 0, 0, 0, 1, 0, 0, 0, 66, 33, 0, 44, 0, 12, 0, 0,
                0, 1, 0, 0, 0, 67, 65
        };

        List<Byte> data = new ArrayList<>();
        SerializationUtils.serialize(c, data);
        System.out.println(Arrays.toString(testData));
        System.out.println(data);
        assertArrayEquals(testData, data.toArray(new Byte[0]));
    }

    @Test
    void serializeArray() {
        @ExonumStruct
        class DD {
            @OrderId(id = 1)
            private short a;

            private DD(short a) {
                this.a = a;
            }
        }

        @ExonumStruct
        class B {
            @OrderId(id = 1)
            private ExonumSlice<DD> b;

            @OrderId(id = 2)
            private String c;

            private B(ExonumSlice<DD> b, String c) {
                this.b = b;
                this.c = c;
            }
        }

        B b = new B(
                new ExonumSlice<>(DD.class, Arrays.asList(new DD((short) 10), new DD((short) 20), new DD((short) 30))),
                "\uD83D\uDC4D");

        List<Byte> bytes = new ArrayList<>();
        SerializationUtils.serialize(b, bytes);

        Byte[] testData = {
                16, 0, 0, 0, 3, 0, 0, 0, 46, 0, 0, 0, 4, 0, 0, 0, 40, 0, 0, 0, 2, 0, 0, 0, 42, 0, 0, 0, 2,
                0, 0, 0, 44, 0, 0, 0, 2, 0, 0, 0, 10, 0, 20, 0, 30, 0, (byte)240, (byte)159, (byte)145, (byte)141};

        System.out.println(Arrays.toString(testData));
        System.out.println(bytes);

        assertArrayEquals(testData, bytes.toArray(new Byte[0]));
    }

    @Test
    void shadowTest() {
        @ExonumStruct
        class Kek {
            transient int shadow$kek = 10;
        }

        List<Byte> bytes = new ArrayList<>();
        SerializationUtils.serialize(new Kek(), bytes);
        assertTrue(bytes.isEmpty());
    }

    @Test
    void serializeSimpleStructureWithShadow() {
        @ExonumStruct
        class SimpleStructure {
            @OrderId(id = 1)
            private String b;

            @OrderId(id = 2)
            private int a;
            transient private int shadow$aaaa = 11;

            private SimpleStructure(int a, String b) {
                this.a = a;
                this.b = b;
            }
        }

        SimpleStructure structure = new SimpleStructure(10, "AB");

        List<Byte> data = new ArrayList<>();

        SerializationUtils.SegmentInfo info = SerializationUtils.serialize(structure, data);

        assertEquals(0, info.getStart());
        assertEquals(14, info.getLength());

        Byte[] testData = {12, 0, 0, 0, 2, 0, 0, 0, 10, 0, 0, 0, 65, 66};
        assertArrayEquals(testData, data.toArray(new Byte[]{}));
    }

    @Test
    void serializeSimpleStructureReversed() {
        @ExonumStruct
        class SimpleStructure {
            @OrderId(id = 2)
            private int a;

            @OrderId(id = 1)
            private String b;

            private SimpleStructure(int a, String b) {
                this.a = a;
                this.b = b;
            }
        }

        SimpleStructure structure = new SimpleStructure(10, "AB");

        List<Byte> data = new ArrayList<>();

        SerializationUtils.SegmentInfo info = SerializationUtils.serialize(structure, data);

        assertEquals(0, info.getStart());
        assertEquals(14, info.getLength());

        Byte[] testData = {12, 0, 0, 0, 2, 0, 0, 0, 10, 0, 0, 0, 65, 66};
        assertArrayEquals(testData, data.toArray(new Byte[]{}));
    }

    @ExonumStruct
    private static class SimpleStructure {
        @OrderId(id = 2)
        private int a;

        @OrderId(id = 1)
        private String b;

        private static long GLOBAL_ID = 10;

        private SimpleStructure(int a, String b) {
            this.a = a;
            this.b = b;
        }
    }

    @Test
    void serializeSimpleStructureWithStatic() {
        SimpleStructure structure = new SimpleStructure(10, "AB");

        List<Byte> data = new ArrayList<>();

        SerializationUtils.SegmentInfo info = SerializationUtils.serialize(structure, data);

        assertEquals(0, info.getStart());
        assertEquals(14, info.getLength());

        Byte[] testData = {12, 0, 0, 0, 2, 0, 0, 0, 10, 0, 0, 0, 65, 66};
        assertArrayEquals(testData, data.toArray(new Byte[]{}));
    }

    @ExonumStruct
    private static class EmptyStruct {
        private static String a = "aaaa";
    }

    @Test
    void serializeSimpleStructureWithStatic2() {
        List<Byte> data = new ArrayList<>();

        SerializationUtils.serialize(new EmptyStruct(), data);

        assertTrue(data.isEmpty());
    }
}