package zinoviy23.lightclient.types;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AbstractHashTest {

    @Test
    void ctorTest() {
        AbstractHash newHash = new AbstractHash("a32acf") {};
        AbstractHash otherHash = new AbstractHash(newHash.getBytes()) {};
        assertEquals(newHash.getHex(), otherHash.getHex());
        assertArrayEquals(newHash.getBytes(), otherHash.getBytes());
    }
}