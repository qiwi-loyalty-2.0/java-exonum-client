package zinoviy23.lightclient.requests;

import org.junit.jupiter.api.Test;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.*;

class ParametersBuilderTest {
    @Test
    void getParamsEncoded() throws UnsupportedEncodingException {
        HashMap<String, String> testCase = new LinkedHashMap<>();
        testCase.put("kek", "bob");
        testCase.put("aaa", "lol");

        String res = ParametersBuilder.getParamsEncoded(testCase);
        assertEquals("kek=bob&aaa=lol", res);
    }

    @Test
    void getParamsEncoded2() throws UnsupportedEncodingException {
        HashMap<String, String> testCase = new LinkedHashMap<>();
        testCase.put("message ", "Саша дурак");

        String res = ParametersBuilder.getParamsEncoded(testCase);
        assertEquals("message+=%D0%A1%D0%B0%D1%88%D0%B0+%D0%B4%D1%83%D1%80%D0%B0%D0%BA", res);
    }
}