package zinoviy23.lightclient.requests;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import zinoviy23.lightclient.transactions.PreparedTransaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.*;

class RequestBuilderTest {
    @Disabled
    @Test
    void check() throws ExecutionException, InterruptedException {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("q", "iterator");

        Future<String> res = RequestBuilder.endPoint("http://www.cplusplus.com/search.do")
                .get()
                .parameters(parameters)
                .onSuccess(new RequestBuilder.OnSuccessHandler<String>() {
                    @Override
                    public String onSuccess(InputStreamReader inputStreamReader) throws IOException {
                        BufferedReader reader = new BufferedReader(inputStreamReader);
                        String line;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line);
                        }

                        return stringBuilder.toString();
                    }
                })
                .onError(new RequestBuilder.OnErrorHandler() {
                    @Override
                    public void onError(Exception ex) {
                        fail(ex.getMessage());
                    }
                })
                .send();

        String result = res.get();
        assertNotNull(result);
        assertTrue(result.contains("html"));
    }

    @Test
    void primitiveTestGet() {
        RequestBuilder builder = RequestBuilder.endPoint("Kek.com");
        assertEquals("Kek.com", builder.getEndPointAddress());

        RequestBuilder.RequestWithEndPoint<Map<String, String>> withEndPoint = builder.get();
        assertEquals("", withEndPoint.getContentType());
        assertEquals("GET", withEndPoint.getMethod().toString());
    }

    @Test
    void primitiveTestPost() {
        RequestBuilder builder = RequestBuilder.endPoint("Kek.com");
        assertEquals("Kek.com", builder.getEndPointAddress());

        RequestBuilder.RequestWithEndPoint<PreparedTransaction> withEndPoint = builder.postTransaction();
        assertEquals("application/json", withEndPoint.getContentType());
        assertEquals("POST", withEndPoint.getMethod().toString());
    }

    @Test
    void errors() {
        RequestBuilder.endPoint("lelele").get()
                .parameters(Collections.<String, String>emptyMap())
                .onSuccess(new RequestBuilder.OnSuccessHandler<String>() {
                    @Override
                    public String onSuccess(InputStreamReader inputStreamReader) throws IOException {
                        return "";
                    }
                })
                .onError(new RequestBuilder.OnErrorHandler() {
                    @Override
                    public void onError(Exception ex) {
                        assertNotNull(ex);
                    }
                })
                .send();
    }
}