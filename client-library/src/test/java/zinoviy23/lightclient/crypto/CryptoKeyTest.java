package zinoviy23.lightclient.crypto;

import org.junit.jupiter.api.Test;
import zinoviy23.lightclient.serialization.OrderId;
import zinoviy23.lightclient.transactions.Transaction;
import zinoviy23.lightclient.types.ExonumSlice;
import zinoviy23.lightclient.types.ExonumStruct;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unused")
class CryptoKeyTest {
    @Test
    void testSign() {
        CryptoKeyPair pair = new CryptoKeyPair(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoSecretKey("978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b" +
                        "fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"));

        class SendFunds extends Transaction {
            @OrderId(id = 1)
            private CryptoPublicKey from;
            @OrderId(id = 2)
            private CryptoPublicKey to;
            @OrderId(id = 3)
            private long amount;

            private SendFunds(CryptoPublicKey from, CryptoPublicKey to, long amount) {
                super((byte) 0, (byte) 0, (short) 128, (short) 130);

                this.from = from;
                this.to = to;
                this.amount = amount;
            }
        }

        SendFunds tx = new SendFunds(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoPublicKey("f7ea8fd02cb41cc2cd45fd5adc89ca1bf605b2e31f796a3417ddbcd4a3634647"),
                1000);

        CryptoSignature sign = pair.getPrivateKey().sign(tx);

        String testHexSignature =
                "c304505c8a46ca19454ff5f18335d520823cd0eb984521472ec7638b312a0f" +
                        "5b1180a3c39a50cbe3b68ed15023c6761ed1495da648c7fe484876f92a659ee10a";

        assertEquals(testHexSignature, sign.getHex());
    }

    @Test
    void testSign2() {
        CryptoKeyPair pair = new CryptoKeyPair(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoSecretKey("978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b" +
                        "fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"));

        @ExonumStruct
        class Wrapper {
            @OrderId(id = 1)
            private String val;

            private Wrapper(String val) {
                this.val = val;
            }
        }

        class SendArray extends Transaction {
            @OrderId(id = 1)
            private CryptoPublicKey from;
            @OrderId(id = 2)
            private CryptoPublicKey to;
            @OrderId(id = 3)
            private ExonumSlice<Wrapper> arr;

            private SendArray(CryptoPublicKey from, CryptoPublicKey to, ExonumSlice<Wrapper> arr) {
                super((byte) 0, (byte) 0, (short) 128, (short) 130);
                this.from = from;
                this.to = to;
                this.arr = arr;
            }
        }

        SendArray sendArray = new SendArray(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoPublicKey("f7ea8fd02cb41cc2cd45fd5adc89ca1bf605b2e31f796a3417ddbcd4a3634647"),
                new ExonumSlice<>(Wrapper.class, Arrays.asList(
                        new Wrapper("Sasha"), new Wrapper("Poly"), new Wrapper("Durachki")))
        );

        CryptoSignature signature = pair.getPrivateKey().sign(sendArray);

        String testSignature = "6494d47bd1147bbffa136bd68a987e4061ffefc759c4351967da72d7a455ac7ad3dfce9c934eeb7" +
                "ec8667ceb39a71b5ae2a0846eed00208fa62d57f9554b600f";

        assertEquals(testSignature, signature.getHex());
    }

    @Test
    void testVerify() {
        CryptoKeyPair pair = new CryptoKeyPair(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoSecretKey("978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b" +
                        "fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"));

        class SendFunds extends Transaction {
            @OrderId(id = 1)
            private CryptoPublicKey from;

            @OrderId(id = 2)
            private CryptoPublicKey to;

            @OrderId(id = 3)
            private long amount;

            private SendFunds(CryptoPublicKey from, CryptoPublicKey to, long amount) {
                super((byte) 0, (byte) 0, (short) 128, (short) 130);

                this.from = from;
                this.to = to;
                this.amount = amount;
            }
        }

        SendFunds tx = new SendFunds(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoPublicKey("f7ea8fd02cb41cc2cd45fd5adc89ca1bf605b2e31f796a3417ddbcd4a3634647"),
                1000);

        CryptoSignature testSignature = new CryptoSignature(
                "c304505c8a46ca19454ff5f18335d520823cd0eb984521472ec7638b312a0f" +
                "5b1180a3c39a50cbe3b68ed15023c6761ed1495da648c7fe484876f92a659ee10a");

        assertTrue(pair.getPublicKey().verify(testSignature, tx));
    }

    @Test
    void testVerify2() {
        CryptoKeyPair pair = new CryptoKeyPair(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoSecretKey("978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b" +
                        "fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"));

        @ExonumStruct
        class Wrapper {
            @OrderId(id = 1)
            private String val;

            private Wrapper(String val) {
                this.val = val;
            }
        }

        class SendArray extends Transaction {
            @OrderId(id = 1)
            private CryptoPublicKey from;
            @OrderId(id = 2)
            private CryptoPublicKey to;
            @OrderId(id = 3)
            private ExonumSlice<Wrapper> arr;

            private SendArray(CryptoPublicKey from, CryptoPublicKey to, ExonumSlice<Wrapper> arr) {
                super((byte) 0, (byte) 0, (short) 128, (short) 130);
                this.from = from;
                this.to = to;
                this.arr = arr;
            }
        }

        SendArray sendArray = new SendArray(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoPublicKey("f7ea8fd02cb41cc2cd45fd5adc89ca1bf605b2e31f796a3417ddbcd4a3634647"),
                new ExonumSlice<>(Wrapper.class, Arrays.asList(
                        new Wrapper("Sasha"), new Wrapper("Poly"), new Wrapper("Durachki")))
        );

        CryptoSignature testSignature = new CryptoSignature(
                "6494d47bd1147bbffa136bd68a987e4061ffefc759c4351967da72d7a455ac7ad3dfce9c934eeb7" +
                        "ec8667ceb39a71b5ae2a0846eed00208fa62d57f9554b600f"
        );

        assertTrue(pair.getPublicKey().verify(testSignature, sendArray));
    }

    @Test
    void signAndVerify() {
        CryptoKeyPair pair = new CryptoKeyPair(
                new CryptoPublicKey("fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"),
                new CryptoSecretKey("978e3321bd6331d56e5f4c2bdb95bf471e95a77a6839e68d4241e7b0932ebe2b" +
                        "fa7f9ee43aff70c879f80fa7fd15955c18b98c72310b09e7818310325050cf7a"));

        @ExonumStruct
        class Test {
            @OrderId(id = 1)
            private int a;

            @OrderId(id = 2)
            private int b;

            private Test(int a, int b) {
                this.a = a;
                this.b = b;
            }
        }

        Test test = new Test(1, 2);

        CryptoSignature signature = pair.getPrivateKey().sign(test);
        assertTrue(pair.getPublicKey().verify(signature, test));
    }
}