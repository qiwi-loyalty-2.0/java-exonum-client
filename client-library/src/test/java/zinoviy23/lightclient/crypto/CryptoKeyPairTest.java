package zinoviy23.lightclient.crypto;

import org.junit.jupiter.api.Test;
import zinoviy23.lightclient.serialization.OrderId;
import zinoviy23.lightclient.types.ExonumStruct;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unused")
class CryptoKeyPairTest {

    @Test
    void genKeys() {
        for (int i = 0; i < 100; i++) {
            CryptoKeyPair pair = CryptoKeyPair.generateKeyPair();

            @ExonumStruct
            class Test {
                @OrderId(id = 1)
                private int a;

                @OrderId(id = 2)
                private int b;

                private Test(int a, int b) {
                    this.a = a;
                    this.b = b;
                }
            }

            Test test = new Test(1, 2);

            CryptoSignature signature = pair.getPrivateKey().sign(test);
            assertTrue(pair.getPublicKey().verify(signature, test));
        }
    }
}