package zinoviy23.lightclient.crypto;

import zinoviy23.lightclient.types.AbstractHash;
import zinoviy23.lightclient.types.FixedSizeType;
import zinoviy23.lightclient.types.Primitive;

/**
 * Class for SHA hashes
 * @author zinoviy23
 */
@SuppressWarnings("WeakerAccess")
@Primitive
public final class CryptoHash extends AbstractHash implements FixedSizeType {
    public static final int CRYPTO_HASH_SIZE = 32;

    public CryptoHash(String hex) {
        super(checkParameter(hex, hex.length(), CRYPTO_HASH_SIZE * 2, "hex.length"));
    }

    public CryptoHash(byte[] bytes) {
        super(checkParameter(bytes, bytes.length, CRYPTO_HASH_SIZE, "bytes.length"));
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public byte[] getBytes() {
        return new byte[0];
    }
}
