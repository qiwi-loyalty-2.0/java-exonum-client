package zinoviy23.lightclient.crypto;

import zinoviy23.lightclient.types.AbstractHash;
import zinoviy23.lightclient.types.FixedSizeType;
import zinoviy23.lightclient.types.Primitive;

/**
 * Class for ED25519 signature
 * @author zinoviy23
 */
@SuppressWarnings("WeakerAccess")
@Primitive
public final class CryptoSignature extends AbstractHash implements FixedSizeType {
    public static final int CRYPTO_SIGNATURE_SIZE = 64;

    public CryptoSignature(String hex) {
        super(checkParameter(hex, hex.length(), CRYPTO_SIGNATURE_SIZE * 2, "hex.length"));
    }

    public CryptoSignature(byte[] bytes) {
        super(checkParameter(bytes, bytes.length, CRYPTO_SIGNATURE_SIZE, "bytes.length"));
    }

    @Override
    public int getSize() {
        return CRYPTO_SIGNATURE_SIZE;
    }
}
