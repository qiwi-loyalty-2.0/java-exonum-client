package zinoviy23.lightclient.crypto;

import com.iwebpp.crypto.TweetNaclFast;
import zinoviy23.lightclient.serialization.SerializationUtils;
import zinoviy23.lightclient.transactions.Transaction;
import zinoviy23.lightclient.types.AbstractHash;
import zinoviy23.lightclient.types.Primitive;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for ED25519 secret keys
 * @author zinoviy23
 */
@SuppressWarnings("WeakerAccess")
@Primitive
public final class CryptoSecretKey extends AbstractHash {
    public static final int CRYPTO_SECRET_KEY_SIZE = 64;

    public CryptoSecretKey(String hex) {
        super(checkParameter(hex, hex.length(), CRYPTO_SECRET_KEY_SIZE * 2, "hex.length"));
    }

    public CryptoSecretKey(byte[] bytes) {
        super(checkParameter(bytes, bytes.length, CRYPTO_SECRET_KEY_SIZE, "bytes.length"));
    }

    /**
     * Signs data by key
     * @param data data
     * @return signature
     */
    public CryptoSignature sign(Object data) {
        TweetNaclFast.Signature objectToSign = new TweetNaclFast.Signature(new byte[0], getBytes());

        List<Byte> message = new ArrayList<>();

        if (data instanceof Transaction) {
            message = ((Transaction) data).serialize();
        } else {
            SerializationUtils.serialize(data, message);
        }

        return new CryptoSignature(objectToSign.detached(SerializationUtils.listObBytesToPrimitiveBytes(message)));
    }
}
