package zinoviy23.lightclient.crypto;

import com.iwebpp.crypto.TweetNaclFast;
import zinoviy23.lightclient.serialization.SerializationUtils;
import zinoviy23.lightclient.transactions.Transaction;
import zinoviy23.lightclient.types.AbstractHash;
import zinoviy23.lightclient.types.FixedSizeType;
import zinoviy23.lightclient.types.Primitive;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for ED25519 public keys
 * @author zinoviy23
 */
@SuppressWarnings("WeakerAccess")
@Primitive
public final class CryptoPublicKey extends AbstractHash implements FixedSizeType {
    public static final int CRYPTO_PUBLIC_KEY_SIZE = 32;

    public CryptoPublicKey(String hex) {
        super(checkParameter(hex, hex.length(), CRYPTO_PUBLIC_KEY_SIZE * 2, "hex.length"));
    }

    public CryptoPublicKey(byte[] bytes) {
        super(checkParameter(bytes, bytes.length, CRYPTO_PUBLIC_KEY_SIZE, "bytes.length"));
    }

    @Override
    public int getSize() {
        return CRYPTO_PUBLIC_KEY_SIZE;
    }

    /**
     * Verifies signature
     * @param signature signature
     * @param data data, that was signed
     * @return true, if signature is correct
     */
    public boolean verify(CryptoSignature signature, Object data) {
        TweetNaclFast.Signature objectForVerify = new TweetNaclFast.Signature(getBytes(), new byte[0]);

        List<Byte> message = new ArrayList<>();
        if (data instanceof Transaction) {
            message = ((Transaction) data).serialize();
        } else {
            SerializationUtils.serialize(data, message);
        }

        return objectForVerify.detached_verify(SerializationUtils.listObBytesToPrimitiveBytes(message),
                signature.getBytes());
    }
}
