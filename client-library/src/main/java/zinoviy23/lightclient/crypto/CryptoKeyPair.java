package zinoviy23.lightclient.crypto;

import com.iwebpp.crypto.TweetNaclFast;

/**
 * Class for representing pairs of public and private key
 * @author zinoviy23
 */
public class CryptoKeyPair {
    private final CryptoPublicKey publicKey;
    private final CryptoSecretKey privateKey;

    public CryptoKeyPair(CryptoPublicKey publicKey, CryptoSecretKey privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public CryptoPublicKey getPublicKey() {
        return publicKey;
    }

    public CryptoSecretKey getPrivateKey() {
        return privateKey;
    }

    /**
     * Generates random key pair of public and private keys
     * @return pair of keys
     */
    public static CryptoKeyPair generateKeyPair() {
        TweetNaclFast.Signature.KeyPair keyPair = TweetNaclFast.Signature.keyPair();

        return new CryptoKeyPair(
                new CryptoPublicKey(keyPair.getPublicKey()),
                new CryptoSecretKey(keyPair.getSecretKey())
                );
    }
}
