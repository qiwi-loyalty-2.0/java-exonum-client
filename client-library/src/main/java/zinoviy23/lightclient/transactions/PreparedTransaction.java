package zinoviy23.lightclient.transactions;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import zinoviy23.lightclient.crypto.CryptoHash;
import zinoviy23.lightclient.crypto.CryptoPublicKey;
import zinoviy23.lightclient.crypto.CryptoSecretKey;
import zinoviy23.lightclient.crypto.CryptoSignature;
import zinoviy23.lightclient.types.AbstractHash;

import java.lang.reflect.Type;

/**
 * Class for transaction, that ready for sending
 * @author zinoviy23
 */
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class PreparedTransaction {
    private final Transaction body;

    @SerializedName("protocol_version")
    private final byte protocolVersion;

    @SerializedName("message_id")
    private final short messageId;

    @SerializedName("service_id")
    private final short serviceId;

    private final CryptoSignature signature;

    public PreparedTransaction(Transaction body, CryptoSecretKey privateKey) {
        this.body = body;

        protocolVersion = body.getProtocolVersion();
        messageId = body.getMessageId();
        serviceId = body.getServiceId();

        signature = privateKey.sign(body);
    }

    public static final  JsonSerializer<? extends AbstractHash> HASH_SERIALIZER =
            new JsonSerializer<AbstractHash>() {
        @Override
        public JsonElement serialize(AbstractHash src, Type type, JsonSerializationContext context) {
            return new JsonPrimitive(src.getHex());
        }
    };

    public static final GsonBuilder BUILDER_FOR_TX = new GsonBuilder()
            .registerTypeAdapter(CryptoPublicKey.class, HASH_SERIALIZER)
            .registerTypeAdapter(CryptoSignature.class, HASH_SERIALIZER)
            .registerTypeAdapter(CryptoHash.class, HASH_SERIALIZER)
            .setLongSerializationPolicy(LongSerializationPolicy.STRING);

    /**
     * Converts to transaction to Json
     * @return string representation
     */
    public String toJson() {
        // it is necessary for converting Hash not to {hex: ".."}, but to "..."


        return BUILDER_FOR_TX
                .create()
                .toJson(this);
    }

    @Override
    public String toString() {
        return toJson();
    }
}
