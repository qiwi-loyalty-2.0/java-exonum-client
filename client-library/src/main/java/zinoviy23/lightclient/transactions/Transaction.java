package zinoviy23.lightclient.transactions;

import zinoviy23.lightclient.serialization.SerializationUtils;
import zinoviy23.lightclient.crypto.CryptoSignature;
import zinoviy23.lightclient.types.ExonumStruct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for transaction. It has header for exonum and body
 * @author zinoviy23
 */
@SuppressWarnings("WeakerAccess")
@ExonumStruct
public abstract class Transaction {
    private final transient byte networkId;
    private final transient byte protocolVersion;
    private final transient short messageId;
    private final transient short serviceId;
    private transient int payload = 0;

    protected Transaction(byte networkId, byte protocolVersion, short messageId, short serviceId) {
        this.networkId = networkId;
        this.protocolVersion = protocolVersion;
        this.messageId = messageId;
        this.serviceId = serviceId;
    }

    /**
     * Serializes transaction, uses SerializationUtils. It has head and all subclasses fields in body
     * @return List of bytes, representing serialized data
     */
    public final List<Byte> serialize() {
        List<Byte> data = new ArrayList<>(Collections.nCopies(10, (byte) 0));
        data.set(0, networkId);
        data.set(1, protocolVersion);
        SerializationUtils.serialize(messageId, 2, data);
        SerializationUtils.serialize(serviceId, 4, data);
        SerializationUtils.serialize(payload, 6, data);

        SerializationUtils.serialize(this, data, -1);

        payload = CryptoSignature.CRYPTO_SIGNATURE_SIZE + data.size();

        SerializationUtils.serialize(payload, 6, data);

        return data;
    }

    public byte getProtocolVersion() {
        return protocolVersion;
    }

    public short getMessageId() {
        return messageId;
    }

    public short getServiceId() {
        return serviceId;
    }
}
