package zinoviy23.lightclient.requests;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

@SuppressWarnings("WeakerAccess")
public class ParametersBuilder {
    public static String getParamsEncoded(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder encodedParams = new StringBuilder();

        for (Map.Entry<String, String> param : params.entrySet()) {
            encodedParams.append(URLEncoder.encode(param.getKey(), "UTF-8"))
                    .append("=")
                    .append(URLEncoder.encode(param.getValue(), "UTF-8"))
                    .append("&");
        }

        if (encodedParams.length() != 0)
            encodedParams.delete(encodedParams.length() - 1, encodedParams.length());

        return encodedParams.toString();
    }
}
