package zinoviy23.lightclient.requests;

import java.io.InputStreamReader;

public class BadRequestCodeException extends RuntimeException {
    private InputStreamReader errorReader;

    public BadRequestCodeException(InputStreamReader errorReader) {
        this.errorReader = errorReader;
    }

    public BadRequestCodeException(Throwable cause, InputStreamReader errorReader) {
        super(cause);
        this.errorReader = errorReader;
    }

    public BadRequestCodeException(String message, InputStreamReader errorReader) {
        super(message);
        this.errorReader = errorReader;
    }

    public InputStreamReader getErrorReader() {
        return errorReader;
    }
}
