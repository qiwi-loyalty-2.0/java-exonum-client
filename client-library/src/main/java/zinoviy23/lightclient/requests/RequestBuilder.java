package zinoviy23.lightclient.requests;

import zinoviy23.lightclient.transactions.PreparedTransaction;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Builder for async request.
 * <p>
 *     <i>Steps of creating request:</i>
 *     <ol>
 *         <li>Set end point</li>
 *         <li>Set method</li>
 *         <li>Set parameters</li>
 *         <li>Set handler for getting data, if all OK</li>
 *         <li>Set handler for errors</li>
 *         <li><b>Send request!</b></li>
 *     </ol>
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class RequestBuilder {
    /**
     * Thread pool
     */
    private static ExecutorService executorService = Executors.newFixedThreadPool(50);

    /**
     * shutdowns the threads
     */
    public static void shutdown() {
        executorService.shutdown();
    }

    /**
     * End point(address)
     */
    private String endPointAddress;

    /**
     * Gets end point
     * @return end point
     */
    public String getEndPointAddress() {
        return endPointAddress;
    }

    /**
     * Closed ctor
     * @param endPointAddress URL for request
     */
    private RequestBuilder(String endPointAddress) {
        this.endPointAddress = endPointAddress;
    }

    /**
     * <b>Starts chain for creating request.</b>. It is sets address for request.
     * @param endPointAddress address for request
     * @return first chain element
     */
    public static RequestBuilder endPoint(String endPointAddress) {
        return new RequestBuilder(endPointAddress);
    }

    /**
     * Post transaction. It requires prepared transaction
     * @return next element
     */
    public RequestWithEndPoint<PreparedTransaction> postTransaction() {
        return new RequestWithEndPoint<PreparedTransaction>() {
            @Override
            void writeParams(DataOutputStream dataOutputStream) throws IOException {
                dataOutputStream.writeBytes(param.toJson());
            }

            @Override
            void writeParams(StringWriter stringWriter) {
                stringWriter.write(param.toJson());
            }

            @Override
            Method getMethod() {
                return Method.POST;
            }

            @Override
            String getContentType() {
                return "application/json";
            }
        };
    }

    /**
     * Get request. It requires map of parameters
     * @return next step
     */
    public RequestWithEndPoint<Map<String, String>> get() {
        return new RequestWithEndPoint<Map<String, String>>() {
            @Override
            void writeParams(DataOutputStream dataOutputStream) throws IOException {
                dataOutputStream.writeBytes(ParametersBuilder.getParamsEncoded(param));
            }

            @Override
            void writeParams(StringWriter stringWriter) throws IOException {
                stringWriter.write(ParametersBuilder.getParamsEncoded(param));
            }

            @Override
            Method getMethod() {
                return Method.GET;
            }

            @Override
            String getContentType() {
                return "";
            }
        };
    }

    public <T> RequestWithEndPoint<T> post(final String contentType) {
        return new MethodRequest<>(Method.POST, contentType);
    }

    public <T> RequestWithEndPoint<T> put(final String contentType) {
        return new MethodRequest<>(Method.PUT, contentType);
    }

    public <T> RequestWithEndPoint<T> delete(final String contentType) {
        return new MethodRequest<>(Method.DELETE, contentType);
    }

    private class MethodRequest<T> extends RequestWithEndPoint<T> {
        private Method localMethod;

        private String contentType;

        public MethodRequest(Method localMethod, String contentType) {
            this.localMethod = localMethod;
            this.contentType = contentType;
        }

        @Override
        void writeParams(DataOutputStream dataOutputStream) throws IOException {
            dataOutputStream.writeBytes(param.toString());
        }

        @Override
        void writeParams(StringWriter stringWriter) throws IOException {
            stringWriter.write(param.toString());
        }

        @Override
        Method getMethod() {
            return localMethod;
        }

        @Override
        String getContentType() {
            return contentType;
        }
    }

    /**
     * Enum for request methods
     */
    enum Method {
        POST, GET, DELETE, PUT
    }

    /**
     * Class for request, which has address. <b style="color:yellow">second step</b>
     * @param <TParam> type of parameters
     */
    public abstract class RequestWithEndPoint<TParam> implements ConnectionBuilder {
        /**
         * Parameters
         */
        TParam param;

        private RequestWithEndPoint() { }

        /**
         * Sets parameters for request
         * @param param request parameters
         * @return next step
         */
        public RequestWithParameters parameters(TParam param) {
            this.param = param;

            return new RequestWithParameters(this);
        }

        /**
         * Writes parameters
         * @param dataOutputStream place for parameters
         * @throws IOException - if something goes wrong in writing
         */
        abstract void writeParams(DataOutputStream dataOutputStream) throws IOException;

        /**
         * Writes parameters at string
         * @param stringWriter place for parameters
         * @throws IOException - if something goes wrong in writing
         */
        abstract void writeParams(StringWriter stringWriter) throws IOException;

        /**
         * Gets request method
         * @return request method
         */
        abstract Method getMethod();

        /**
         * Gets content type
         * @return request content type
         */
        abstract String getContentType();

        @Override
        public URLConnection buildConnection() throws IOException {
            HttpURLConnection connection;

            if (getMethod() == Method.GET) {
                String params;
                try (StringWriter writer = new StringWriter()) {
                    writeParams(writer);
                    params = writer.toString();
                }

                String newURL = String.format("%s%s%s", endPointAddress, (params.isEmpty()) ? "" : "?", params);
                connection = (HttpURLConnection) new URL(newURL).openConnection();

                connection.setRequestProperty("Content-Type", getContentType());
                connection.setRequestMethod(getMethod().name());
            } else {
                connection  = (HttpURLConnection) new URL(endPointAddress).openConnection();

                connection.setRequestProperty("Content-Type", getContentType());
                connection.setRequestMethod(getMethod().name());

                connection.setDoOutput(true);
                try (DataOutputStream stream = new DataOutputStream(connection.getOutputStream())) {
                    writeParams(stream);
                    stream.flush();
                }
            }

            return connection;
        }

        /**
         * Gets parameters of request
         * @return parameters of request
         */
        public TParam getParam() {
            return param;
        }
    }

    /**
     * Class for request with parameters. <b style="color:yellow">third step</b>
     */
    public final class RequestWithParameters implements ConnectionBuilder{
        /**
         * Previous step
         */
        private RequestWithEndPoint requestWithEndPoint;

        private RequestWithParameters(RequestWithEndPoint requestWithEndPoint) {
            this.requestWithEndPoint = requestWithEndPoint;
        }

        /**
         * Accepts handler for getting data from response, if all ok
         * @param handler OnSuccess handler
         * @param <T> type of returned data
         * @return next step
         */
        public <T> RequestWithOnSuccessHandler<T> onSuccess(OnSuccessHandler<T> handler) {
            return new RequestWithOnSuccessHandler<>(handler, this);
        }

        @Override
        public URLConnection buildConnection() throws IOException {
            return requestWithEndPoint.buildConnection();
        }
    }

    /**
     * Class for request with OnSuccessHandler. <b style="color:yellow">fourth step</b>
     * @param <T> type of returned data
     */
    public final class RequestWithOnSuccessHandler<T> implements ConnectionBuilder {
        private OnSuccessHandler<T> onSuccessHandler;
        private RequestWithParameters parameters;

        private RequestWithOnSuccessHandler(OnSuccessHandler<T> onSuccessHandler, RequestWithParameters parameters) {
            this.onSuccessHandler = onSuccessHandler;
            this.parameters = parameters;
        }

        /**
         * Sets onError handler
         * @param onErrorHandler handler for errors
         * @return next step
         */
        public ReadyRequest<T> onError(OnErrorHandler onErrorHandler) {
            return new ReadyRequest<>(onErrorHandler, this);
        }

        @Override
        public URLConnection buildConnection() throws IOException {
            return parameters.buildConnection();
        }
    }

    /**
     * Ready request. <b style="color:yellow">third step</b>
     * @param <T> type of data, which is gotten from request
     */
    public final class ReadyRequest<T> {
        private RequestWithOnSuccessHandler<T> requestWithOnSuccessHandler;
        private OnErrorHandler onErrorHandler;

        private ReadyRequest(OnErrorHandler onErrorHandler, RequestWithOnSuccessHandler<T> requestWithOnSuccessHandler) {
            this.requestWithOnSuccessHandler = requestWithOnSuccessHandler;
            this.onErrorHandler = onErrorHandler;
        }

        /**
         * Send async request. If returns Optional for handling if something gones wrong
         * @return object for getting data.
         */
        public Future<T> send() {
            return executorService.submit(new Callable<T>() {
                @Override
                public T call() throws Exception {
                    InputStreamReader reader = null;
                    try {
                        HttpURLConnection connection = (HttpURLConnection) requestWithOnSuccessHandler
                                .buildConnection();

                        int statusCode = connection.getResponseCode();

                        if (statusCode < 200 || statusCode >= 400) {
                            reader = new InputStreamReader(connection.getErrorStream());
                            throw new BadRequestCodeException("Status code " + statusCode, reader);
                        }

                        reader = new InputStreamReader(connection.getInputStream());
                        return requestWithOnSuccessHandler.onSuccessHandler
                                .onSuccess(reader);
                    } catch (IOException | BadRequestCodeException e) {
                        onErrorHandler.onError(e);
                        return null;
                    } finally {
                        if (reader != null) {
                            reader.close();
                        }
                    }
                }
            });
        }
    }

    /**
     * Interface for error handling
     */
    public interface OnErrorHandler {
        /**
         * Handles exception
         * @param ex type of error
         */
        void onError(Exception ex);
    }

    /**
     * Interface for getting data from response
     * @param <T> needed data type
     */
    public interface OnSuccessHandler<T> {
        /**
         * Gets data from response
         * @param responseInputStream response input stream
         * @return data from response
         * @throws IOException if something wrong with reading-writing
         */
        T onSuccess(InputStreamReader responseInputStream) throws IOException;
    }

    /**
     * Interface for building connection
     */
    private interface ConnectionBuilder {
        /**
         * builds connection
         * @return return built connection
         * @throws IOException if something happened whong, while connection was building
         */
        URLConnection buildConnection() throws IOException;
    }
}
