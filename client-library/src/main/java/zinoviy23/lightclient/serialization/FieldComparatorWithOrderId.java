package zinoviy23.lightclient.serialization;

import java.lang.reflect.Field;
import java.util.Comparator;

/**
 * Compares to fields, annotated by OrderId. If someone doesn't has this annotation, than it is greater.
 * So, firstly goes annotated by OrderId sorted by id, then goes non-annotated in some order
 * (It hasn't to be a declaration order).
 */
class FieldComparatorWithOrderId implements Comparator<Field> {
    private final static FieldComparatorWithOrderId instance = new FieldComparatorWithOrderId();

    /**
     * Gets instance of comparator
     * @return instance of comparator
     */
    static FieldComparatorWithOrderId getInstance() {
        return instance;
    }

    @Override
    public int compare(Field o1, Field o2) {
        OrderId orderId1 = o1.getAnnotation(OrderId.class);
        OrderId orderId2 = o2.getAnnotation(OrderId.class);

        if (orderId1 != null && orderId2 != null)
            return Integer.compare(orderId1.id(), orderId2.id());
        else if (orderId1 != null)
            return -1;
        else if (orderId2 != null)
            return 1;

        return 0;
    }

    private FieldComparatorWithOrderId() {}


}
