package zinoviy23.lightclient.serialization;

import zinoviy23.lightclient.types.ExonumSlice;
import zinoviy23.lightclient.types.ExonumStruct;
import zinoviy23.lightclient.types.FixedSizeType;
import zinoviy23.lightclient.types.Primitive;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Class for serializing data by exonum rules
 * @author zinoviy23
 */
@SuppressWarnings("WeakerAccess")
public class SerializationUtils {
    /**
     * Segment info for exonum serializing
     */
    public static class SegmentInfo {
        /**
         * Start of var-length data
         */
        private final int start;

        /**
         * Length of data
         */
        private final int length;

        /**
         * Constructor
         * @param start start of data
         * @param length length of data
         */
        public SegmentInfo(int start, int length) {
            this.start = start;
            this.length = length;
        }

        /**
         * Gets start index
         * @return start index
         */
        public int getStart() {
            return start;
        }

        /**
         * Gets length of data
         * @return length of data
         */
        public int getLength() {
            return length;
        }
    }


    /**
     * Serializes byte value to data
     * @param valueToSerialize byte value
     * @param position position in data
     * @param data List of bytes, must be resized for adding valueToSerialize
     * @throws IndexOutOfBoundsException if position is wrong
     */
    public static void serialize(byte valueToSerialize, int position, List<Byte> data) {
        if (data.size() <= position || position < 0)
            throw new IndexOutOfBoundsException("position index not in data!");

        data.set(position, valueToSerialize);
    }

    /**
     * Serializes short value to data
     * @param valueToSerialize short value
     * @param position position in data
     * @param data List of bytes, must be resized for adding valueToSerialize
     * @throws IndexOutOfBoundsException if position is wrong
     */
    public static void serialize(short valueToSerialize, int position, List<Byte> data) {
        if (data.size() <= position + 1 || position < 0)
            throw new IndexOutOfBoundsException("[position, position + 1] segment not in data!");

        for (int i = 1; i >= 0; i--, valueToSerialize >>= 8)
            data.set(position + 1 - i, (byte) (valueToSerialize & 0xFF));
    }

    /**
     * Serializes int value to data
     * @param valueToSerialize int value
     * @param position position in data
     * @param data List of bytes, must be resized for adding valueToSerialize
     * @throws IndexOutOfBoundsException if position is wrong
     */
    public static void serialize(int valueToSerialize, int position, List<Byte> data) {
        if (data.size() <= position + 3 || position < 0)
            throw new IndexOutOfBoundsException("[position, position + 3] segment not in data!");

        for (int i = 3; i >= 0; i--, valueToSerialize >>= 8)
            data.set(position + 3 - i, (byte) (valueToSerialize & 0xFF));
    }

    /**
     * Serializes long value to data
     * @param valueToSerialize long value
     * @param position position in data
     * @param data List of bytes, must be resized for adding valueToSerialize
     * @throws IndexOutOfBoundsException if position is wrong
     */
    public static void serialize(long valueToSerialize, int position, List<Byte> data) {
        if (data.size() <= position + 7 || position < 0)
            throw new IndexOutOfBoundsException("[position, position + 7] segment not in data!");

        for (int i = 7; i >= 0; i--, valueToSerialize >>= 8)
            data.set(position + 7 - i, (byte) (valueToSerialize & 0xFF));
    }

    /**
     * Serializes boolean value to data
     * @param valueToSerialize boolean value
     * @param position position in data
     * @param data List of bytes, must be resized for adding valueToSerialize
     * @throws IndexOutOfBoundsException if position is wrong
     */
    public static void serialize(boolean valueToSerialize, int position, List<Byte> data) {
        serialize((byte) (valueToSerialize ? 0x01 : 0x00), position, data);
    }

    /**
     * Serializes AbstractHash value, that has fixed size to data
     * @param valueToSerialize hash value
     * @param position position in data
     * @param data List of bytes, must be resized for adding valueToSerialize
     * @throws IndexOutOfBoundsException if position is wrong
     */
    public static void serialize(FixedSizeType valueToSerialize, int position, List<Byte> data) {
        if (data.size() <= position + valueToSerialize.getSize() - 1 || position < 0)
            throw new IndexOutOfBoundsException(String.format("[position, position + %d] segment not in data!",
                    valueToSerialize.getSize()));

        byte[] bytesToAdd = valueToSerialize.getBytes();

        for (int i = 0; i < bytesToAdd.length; i++)
            data.set(position + i, bytesToAdd[i]);
    }

    public static class SegmentInfoForSlice extends SegmentInfo {
        /**
         * Elements count at slice
         */
        private final int elementsCount;

        /**
         * Constructor
         * @param start  start of data
         * @param length length of data
         * @param elementsCount count of elements at slice
         */
        public SegmentInfoForSlice(int start, int length, int elementsCount) {
            super(start, length);
            this.elementsCount = elementsCount;
        }

        public int getElementsCount() {
            return elementsCount;
        }
    }

    /**
     * Serializes exonum list to data
     * @param listOfValuesToSerialize exomum list
     * @param data list of bytes
     * @param parentObjectSize current size of parent object
     * @return segment pointers and element counts
     */
    public static SegmentInfoForSlice serialize(ExonumSlice listOfValuesToSerialize, List<Byte> data,
                                                int parentObjectSize) {
        final int start = data.size();
        int newParentObjectSize = parentObjectSize;
        data.addAll(Collections.nCopies(listOfValuesToSerialize.size() * 8, ((byte)0)));
        int currentPos = start;

        for (Object object : listOfValuesToSerialize) {
            SegmentInfo info;
            if (isPrimitive(listOfValuesToSerialize.getElementType())) {
                info = addPrimitiveAsNotPrimitive(object, data, newParentObjectSize);
            } else if (listOfValuesToSerialize.getElementType().equals(ExonumSlice.class)) {
                SegmentInfoForSlice infoForSlice = serializeAsPart((ExonumSlice) object, data, newParentObjectSize,
                        currentPos);

                newParentObjectSize += infoForSlice.getLength();
                currentPos += 8;
                continue;
            } else {
                info = serialize(object, data, data.size());
            }

            serialize(info.getStart(), currentPos, data);
            serialize(info.getLength(), currentPos + 4, data);

            newParentObjectSize += info.getLength();
            currentPos += 8;
        }

        return new SegmentInfoForSlice(parentObjectSize, data.size() - start, listOfValuesToSerialize.size());
    }

    /**
     * Adds primitive object, but like fixed size (into the body)
     * @param primitive object of primitive type
     * @param data list of buffers
     * @param parentObjectSize current size of parent object
     * @return segment pointers
     */
    private static SegmentInfo addPrimitiveAsNotPrimitive(Object primitive, List<Byte> data, int parentObjectSize) {
        final int start = data.size();

        if (primitive instanceof Byte) {
            data.add((byte) 0);
            serialize((byte) primitive, start, data);
        } else if (primitive instanceof Short) {
            data.addAll(Collections.nCopies(2, (byte)(0)));
            serialize((short) primitive, start, data);
        } else if (primitive instanceof Integer) {
            data.addAll(Collections.nCopies(4, (byte)(0)));
            serialize((int) primitive, start, data);
        } else if (primitive instanceof Long) {
            data.addAll(Collections.nCopies(8, (byte)(0)));
            serialize((long) primitive, start, data);
        } else if (primitive instanceof Boolean) {
            data.add((byte) 0);
            serialize((boolean) primitive, start, data);
        } else if (primitive.getClass().isAnnotationPresent(Primitive.class) && primitive instanceof FixedSizeType) {
            data.addAll(Collections.nCopies(((FixedSizeType) primitive).getSize(), (byte) 0));
            serialize((FixedSizeType) primitive, start, data);
        } else {
            throw new UnsupportedOperationException(String.format("%s is not primitive type!",
                                                                  primitive.getClass()));
        }

        return new SegmentInfo(parentObjectSize, data.size() - start);
    }

    /**
     * Serialize String, by adding it to end
     * @param stringToSerialize string value
     * @param data List of bytes
     * @return segment info
     */
    public static SegmentInfo serialize(String stringToSerialize, List<Byte> data) {
        return serialize(stringToSerialize, data, 0);
    }

    /**
     * Serialize String, by adding it to end
     * @param stringToSerialize string value
     * @param data List of bytes
     * @param parentObjectSize size of head + current body of parent object
     * @return segment info
     */
    public static SegmentInfo serialize(String stringToSerialize, List<Byte> data, int parentObjectSize) {
        byte[] bytesToAppend = stringToSerialize.getBytes(Charset.forName("UTF-8"));

        for (byte b : bytesToAppend)
            data.add(b);

        return new SegmentInfo(parentObjectSize, bytesToAppend.length);
    }

    /**
     * Serializes object. <b style="color:red">Use @OrderId for fields</b> <br>
     *     It sorts fields by their OrderId. If field has not OrderId, than it will be after all annotated fields.
     * @param value object
     * @param data List of bytes
     * @return segment pointers
     * @see OrderId
     */
    public static SegmentInfo serialize(Object value, List<Byte> data) {
        return serialize(value, data, 0);
    }

    /**
     * Serializes object. <b style="color:red">Use @OrderId for fields</b> <br>
     *      It sorts fields by their OrderId. If field has not OrderId, than it will be after all annotated fields.
     * @param value object
     * @param data List of bytes
     * @param parentObjectSize size of head + current body of parent object. Pass smth less than 0, to indicate,
     *                         that it has not parent object
     * @return segment pointers
     * @see OrderId
     */
    public static SegmentInfo serialize(Object value, List<Byte> data, int parentObjectSize) {
        throwIfNotTypeForSerialization(value.getClass());

        final int start = data.size();
        int currentObjectSize = calculateSizeOfHeader(value);
        data.addAll(Collections.nCopies(currentObjectSize, (byte) 0));
        currentObjectSize += (parentObjectSize < 0 ? start : 0);

        Field[] fields = value.getClass().getDeclaredFields();
        Arrays.sort(fields, FieldComparatorWithOrderId.getInstance());

        int position = start;
        for (Field field : fields) {
            if (field.getType().equals(value.getClass().getEnclosingClass()))
                continue;
            if (Modifier.isTransient(field.getModifiers()) || Modifier.isStatic(field.getModifiers()))
                continue;

            try {
                if (!field.isAccessible()) // if field is private
                    field.setAccessible(true);

                Object fieldValue = field.get(value);
                Class type = field.getType();

                if (type.equals(Byte.class) || type.equals(byte.class)) {
                    serialize((byte) fieldValue, position, data);
                    position++;
                } else if (type.equals(Short.class) || type.equals(short.class)) {
                    serialize((short) fieldValue, position, data);
                    position += 2;
                } else if (type.equals(Integer.class) || type.equals(int.class)) {
                    serialize((int) fieldValue, position, data);
                    position += 4;
                } else if (type.equals(Long.class) || type.equals(long.class)) {
                    serialize((long) fieldValue, position, data);
                    position += 8;
                } else if (type.equals(Boolean.class) || type.equals(boolean.class)) {
                    serialize((boolean) fieldValue, position, data);
                    position += 1;
                } else if (fieldValue instanceof FixedSizeType) {
                    serialize((FixedSizeType) fieldValue, position, data);
                    position += ((FixedSizeType) fieldValue).getSize();
                } else if (type.equals(ExonumSlice.class)) {
                    SegmentInfoForSlice segmentInfoForSlice =
                            serializeAsPart((ExonumSlice) fieldValue, data, currentObjectSize, position);

                    currentObjectSize += segmentInfoForSlice.getLength();
                    position += 8;
                } else if (type.equals(String.class) || type.isAnnotationPresent(ExonumStruct.class)) {
                    SegmentInfo segmentInfo = (type.equals(String.class)) ?
                            serialize((String) fieldValue, data, currentObjectSize) :
                            serialize(fieldValue, data, currentObjectSize);

                    serialize(segmentInfo.start, position, data);
                    serialize(segmentInfo.length, position + 4, data);
                    position += 8;
                    currentObjectSize += segmentInfo.length;
                } else {
                    throw new UnsupportedOperationException(String.format("%s is unsupported type!", type.getName()));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return new SegmentInfo(parentObjectSize, data.size() - start);
    }

    /*
        Serialize slice as part of other structure
     */
    private static SegmentInfoForSlice serializeAsPart(ExonumSlice slice, List<Byte> data, int currentObjectSize,
                                                       int position) {
        SegmentInfoForSlice segmentInfoForSlice = serialize(slice, data,
                currentObjectSize);

        serialize(segmentInfoForSlice.getStart(), position, data);
        serialize(segmentInfoForSlice.getElementsCount(), position + 4, data);

        return segmentInfoForSlice;
    }

    /**
     * Gets size of object, if it in header
     * @param value object
     * @return count of bytes, that this object occupies
     */
    private static int getSizeAtHeader(Object value) {
        if (value instanceof FixedSizeType)
            return ((FixedSizeType) value).getSize();

        if (value instanceof Byte || value instanceof Boolean)
            return 1;

        if (value instanceof Short)
            return 2;

        if (value instanceof Integer)
            return 4;

        if (value instanceof Long)
            return 8;

        if (value instanceof String)
            return 8;

        if (!value.getClass().isAnnotationPresent(ExonumStruct.class))
            throw new UnsupportedOperationException(String.format(
                    "All types must be primitives or ExonumStruct! %s is unsupported type",
                    value.getClass().getName()));

        return 8; // var-length type, so 8 bytes for header
    }

    /**
     * Calculate size of header.
     * @param object object, which header size is needed
     * @return number of bytes
     */
    private static int calculateSizeOfHeader(Object object) {
        throwIfNotTypeForSerialization(object.getClass());

        Field[] objectFields = object.getClass().getDeclaredFields();
        Arrays.sort(objectFields, FieldComparatorWithOrderId.getInstance());

        int result = 0;
        for (Field field : objectFields) {
            if (field.getType().equals(object.getClass().getEnclosingClass()))
                continue;
            if (Modifier.isTransient(field.getModifiers()) || Modifier.isStatic(field.getModifiers()))
                continue;

            try {
                if (!field.isAccessible())  // if field is private
                    field.setAccessible(true);

                result += getSizeAtHeader(field.get(object));
            } catch (IllegalAccessException e) { // private fields are not serializing
                e.printStackTrace();
            }
        }

        return result;
    }

    /**
     * Throws exception, if type not supported
     * @param type type for checking
     * @throws UnsupportedOperationException if type unsupported
     */
    private static void throwIfNotTypeForSerialization(Class type) {
        if (!(type.isAnnotationPresent(ExonumStruct.class)
                || isPrimitive(type)
                || type.equals(String.class)))
            throw new UnsupportedOperationException(
                    String.format("%s is unsupported type! Supported is ExonumType and primitives",
                                  type.getName()));
    }

    /**
     * Checks, that type is primitive in exonum
     * @param type class
     * @return true, if primitive
     */
    private static boolean isPrimitive(Class type) {
        return (type.isAnnotationPresent(Primitive.class) && type.equals(ExonumStruct.class))
                || type.equals(Integer.class) || type.equals(int.class)
                || type.equals(Byte.class) || type.equals(byte.class)
                || type.equals(Long.class) || type.equals(long.class)
                || type.equals(Short.class) || type.equals(short.class)
                || type.equals(Boolean.class) || type.equals(boolean.class);

    }

    /**
     * Converts Byte list to byte array
     * @param bytes list of Byte
     * @return array of byte
     */
    public static byte[] listObBytesToPrimitiveBytes(List<Byte> bytes) {
        byte[] result = new byte[bytes.size()];

        for (int i = 0; i < result.length; i++)
            result[i] = bytes.get(i);

        return result;
    }
}
