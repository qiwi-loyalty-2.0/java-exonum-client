package zinoviy23.lightclient.serialization;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for marking field for their ordering
 * @author zinoviy23
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderId {
    /**
     * Index in ordered list of field, annotated by this
     * @return index
     */
    int id();
}
