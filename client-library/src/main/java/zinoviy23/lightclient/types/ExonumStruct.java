package zinoviy23.lightclient.types;

import java.lang.annotation.*;

/**
 * To annotate, that class is exonum sturct
 * @author zinoviy23
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ExonumStruct { }
