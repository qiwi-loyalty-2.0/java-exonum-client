package zinoviy23.lightclient.types;

/**
 * Interface for types, that have constant size
 * @author zinoviy23
 */
public interface FixedSizeType {
    /**
     * Gets size of object. It must return const value for all object of type.
     * @return number of bytes for representing this object
     */
    int getSize();

    /**
     * Gets bytes of structure. It will be great, if its length is equals to getSize() value.
     * @return array of bytes.
     */
    byte[] getBytes();
}
