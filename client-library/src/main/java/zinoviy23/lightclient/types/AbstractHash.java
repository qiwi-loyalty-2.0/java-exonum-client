package zinoviy23.lightclient.types;

/**
 * Class for hash data
 * @author zinoviy23
 */
@ExonumStruct
public abstract class AbstractHash {
    private final String hex;

    public AbstractHash(String hex) {
        if (hex == null)
            throw new IllegalArgumentException("null");

        if (hex.length() % 2 != 0)
            throw new IllegalArgumentException("hex.length() must be divisible by 2");

        this.hex = hex;
    }

    public AbstractHash(byte[] bytes) {
        StringBuilder sb = new StringBuilder();

        for (byte b : bytes)
        {
            int second = b & 0x0F;
            int first = (b & 0xF0) >> 4;
            sb.append(toHexChar(first)).append(toHexChar(second));
        }

        hex = sb.toString();
    }

    private static char toHexChar(int a) {
        return (char) ((a < 10) ? '0' + a : 'a' + (a - 10));
    }

    public String getHex() {
        return hex;
    }

    public byte[] getBytes() {
        byte[] result = new byte[hex.length() / 2];

        for (int i = 0; i < hex.length(); i += 2)
        {
            int intValue = Integer.parseInt(hex.substring(i, i + 2), 16);
            result[i / 2] = (byte) intValue;
        }

        return result;
    }

    /**
     * Checks, that parameter is equal to expectedValue
     * @param parameter parameter value
     * @param expectedValue expected parameter value
     * @param parameterName parameter name
     * @param <T> type of parameters
     * @param <S> type of return type
     * @param result value, which will be returned
     * @return result
     */
    @SuppressWarnings("SameParameterValue")
    protected static <T, S> S checkParameter(S result, T parameter, T expectedValue, String parameterName) {
        if (!parameter.equals(expectedValue))
            //noinspection MalformedFormatString
            throw new IllegalArgumentException(String.format("%s must be %d, not %d", parameterName,
                                                             expectedValue, parameter));

        return result;
    }

    /**
     * Checks, that parameter is equal to expectedValue
     * @param parameter parameter value
     * @param expectedValue expected parameter value
     * @param parameterName parameter name
     * @param <T> type of parameters
     * @param result value, which will be returned
     * @return result
     */
    @SuppressWarnings("SameParameterValue")
    protected static <T> byte[] checkParameter(byte[] result, T parameter, T expectedValue, String parameterName) {
        if (!parameter.equals(expectedValue))
            //noinspection MalformedFormatString
            throw new IllegalArgumentException(String.format("%s must be %d, not %d", parameterName,
                    expectedValue, parameter));

        return result;
    }

    @Override
    public String toString() {
        return hex;
    }
}
