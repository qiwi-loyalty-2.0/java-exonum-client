package zinoviy23.lightclient.types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * List for representation exonum list. It is readonly
 * @author zinoviy23
 */
@ExonumStruct
public final class ExonumSlice<T> extends ArrayList<T> {
    /**
     * Type of elements
     */
    private final Class<T> elementType;

    /**
     * Constructor, that initialize empty list
     * @param elementType type of elements
     */
    public ExonumSlice(Class<T> elementType) {
        this.elementType = elementType;
    }

    /**
     * Constructor, that initialize list from existing collection
     * @param elementType type of elements
     * @param collection collection with elements
     */
    public ExonumSlice(Class<T> elementType, Collection<? extends T> collection) {
        this.elementType = elementType;
        super.addAll(collection);
    }

    public Class<T> getElementType() {
        return elementType;
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void replaceAll(UnaryOperator<T> operator) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sort(Comparator<? super T> c) {
        throw new UnsupportedOperationException();
    }
}
